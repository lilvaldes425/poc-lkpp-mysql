<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">    
    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/> -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->
    <!-- <link href="{{ asset('assets/datatables/datatables.min.css') }}" rel="stylesheet">    
    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">     -->
    <link href="{{ asset('css/mysidebar.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery.maskedinput.js') }}"></script>
    <!-- <script src="{{ asset('js/pagination.js') }}"></script> -->
    <!-- <script src="{{ asset('assets/datatables/datatables.min.js') }}"></script> -->
    <!-- <script src="{{ asset('../resources/assets/js/app.js') }}"></script> -->
    <!-- <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script> -->

    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> -->

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">

    

</head>
<body>
    <!-- <div id="app" style="position: fixed;width: 100%"> -->
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0px">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <span><img alt="logo" class="img"  style="margin-left: -70px; margin-top: -12px; width: 50px;" src="{{ asset('assets/LogoBening.png') }}"/>{{ config('app.name', 'Laravel') }}</span>
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('profile') }}">
                                            Profile
                                        </a>                                        
                                    </li>
                                    <li>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @guest
        <div id="mainDiv" style="margin-left: 0px">
        @else
        <div id="mySidenav" class="sidenav">
            <span id='openMenu' class='togglebtn' onclick="openNav()">&#9776;</span>
            <span id='closeMenu' class='togglebtn' onclick="closeNav()">&#9776;</span>
            <div id='navMenu'>                
                @if  (Auth::user()->role_id == 1) 
                <span id="dashMenu"><a href="{{route('dashboard')}}"><i style="margin-left: -13px" class="fa fa-apple-alt"></i><span style="margin-left: 10px">Dashboard</span></a></span>
                <span id="dashMenu"><a href="{{route('home')}}"><i style="margin-left: -13px" class="fa fa-user"></i><span style="margin-left: 10px">Manajemen User</span></a></span><span id="dashMenu1"><a href="{{route('home')}}" title="Manajemen User"><i class="fa fa-user" style="margin-left: -13px;"></i></a></span>
                <span id="roleMenu"><a href="{{route('listRole')}}"><i style="margin-left: -13px" class="fa fa-users-cog"></i><span style="margin-left: 10px">Manajemen Role</span></a></span><span id="roleMenu1"><a href="{{route('listRole')}}" title="Manajemen Role"><i class="fa fa-users-cog" style="margin-left: -13px;"></i></a></span>
                <span id="jabMenu"><a href="{{route('listJabatan')}}"><i style="margin-left: -13px" class="fa fa-briefcase"></i><span style="margin-left: 10px">Manajemen Jabatan</span></a></span><span id="jabMenu1"><a href="{{route('listJabatan')}}" title="Manajemen Jabatan"><i class="fa fa-briefcase" style="margin-left: -13px;"></i></a></span>
                <span id="itemMenu"><a href="{{route('listItem')}}"><i style="margin-left: -13px" class="fa fa-apple-alt"></i><span style="margin-left: 10px">Manajemen Barang</span></a></span><span id="itemMenu1"><a href="{{route('listItem')}}" title="Manajemen Barang"><i class="fa fa-apple-alt" style="margin-left: -13px;"></i></a></span>                
                <span id="shopMenu"><a href="{{route('listShop')}}"><i style="margin-left: -13px" class="fa fa-shopping-cart"></i><span style="margin-left: 10px">Beli Barang</span></a></span><span id="shopMenu1"><a href="{{route('listShop')}}" title="Beli Barang"><i class="fa fa-shopping-cart" style="margin-left: -13px;"></i></a></span>
                <span id="transaksiMenu"><a href="{{route('listTransaksi')}}"><i style="margin-left: -13px" class="fa fa-history"></i><span style="margin-left: 10px">Riwayat Transaksi</span></a></span><span id="transaksiMenu1"><a href="{{route('listShop')}}" title="Riwayat Transaksi"><i class="fa fa-history" style="margin-left: -13px;"></i></a></span>
                @else
                    @if  (Auth::user()->role_id == 2) 
                        <span id="dashMenu"><a href="{{route('profile')}}"><i style="margin-left: -13px" class="fa fa-home"></i><span style="margin-left: 10px">Dashboard</span></a></span><span id="dashMenu1"><a href="{{route('profile')}}" title="Dashboard"><i class="fa fa-home" style="margin-left: -13px;"></i></a></span>
                    @else
                        <span id="dashMenu"><a href="{{route('home')}}"><i style="margin-left: -13px" class="fa fa-home"></i><span style="margin-left: 10px">Dashboard</span></a></span><span id="dashMenu1"><a href="{{route('home')}}" title="Dashboard"><i class="fa fa-home" style="margin-left: -13px;"></i></a></span>
                    @endif

                    @if  (Auth::user()->role_id == 3) 
                        <span id="itemMenu"><a href="{{route('listItem')}}"><i style="margin-left: -13px" class="fa fa-apple-alt"></i><span style="margin-left: 10px">Manajemen Barang</span></a></span><span id="itemMenu1"><a href="{{route('listItem')}}" title="Manajemen Barang"><i class="fa fa-apple-alt" style="margin-left: -13px;"></i></a></span> 
                    @endif
                <span id="shopMenu"><a href="{{route('listShop')}}"><i style="margin-left: -13px" class="fa fa-shopping-cart"></i><span style="margin-left: 10px">Beli Barang</span></a></span><span id="shopMenu1"><a href="{{route('listShop')}}" title="Beli Barang"><i class="fa fa-shopping-cart" style="margin-left: -13px;"></i></a></span>
                <span id="transaksiMenu"><a href="{{route('listTransaksi')}}"><i style="margin-left: -13px" class="fa fa-history"></i><span style="margin-left: 10px">Riwayat Transaksi</span></a></span><span id="transaksiMenu1"><a href="{{route('listShop')}}" title="Riwayat Transaksi"><i class="fa fa-history" style="margin-left: -13px;"></i></a></span>
                @endif                
            </div>
        </div>
        <div id="mainDiv" style="margin-left: 220px">
        @endguest

            <div>&nbsp;
            </div>
            
            @yield('content')

            &nbsp;&nbsp;
            <div id="footer">
                <div class="container">
                    <p class="text-muted credit"><span style="text-align: left; float: left">&copy; 2019 PT. Bening Guru Semesta</span> 
                    <span class="hidden-phone" style="text-align: right; float: right">Powered by: Laravel</span></p>
                </div>
                &nbsp;&nbsp;
            </div>
        </div>


    </div>
    

    <script type="text/javascript">
        $(function() {
            $('#openMenu').hide();
            $('#dashMenu1').hide();
            $('#roleMenu1').hide();
            $('#jabMenu1').hide();
            $('#itemMenu1').hide();
            $('#shopMenu1').hide();
            $('#transaksiMenu1').hide();

            $("#npwp").mask("99.999.999.9-999.999");
            $("#phone").mask("?9999999999999");
        });

        function setNpwp(npwp) {
            return npwp.mask("99.999.999.9-999.999");
        }

        function openNav() {
            $("#mySidenav").css("width","220px");
            $("#mainDiv").css("margin-left","220px");
            $('#openMenu').hide();
            $('#closeMenu').show();
            //$('#navMenu').show('fast');
            setTimeout(function() {
                $('#dashMenu1').hide();
                $('#dashMenu').show();
                $('#roleMenu1').hide();
                $('#roleMenu').show();
                $('#jabMenu1').hide();
                $('#jabMenu').show();
                $('#itemMenu1').hide();
                $('#itemMenu').show();
                $('#shopMenu1').hide();
                $('#shopMenu').show();
                $('#transaksiMenu1').hide();
                $('#transaksiMenu').show();
            }, 400);
            
        }

        function closeNav() {
            $("#mySidenav").css("width","60px");
            $("#mainDiv").css("margin-left","0");
            $('#closeMenu').hide();
            $('#openMenu').show();
            //$('#navMenu').hide('fast');
            $('#dashMenu1').show();
            $('#dashMenu').hide();
            $('#roleMenu1').show();
            $('#roleMenu').hide();
            $('#jabMenu1').show();
            $('#jabMenu').hide();
            $('#itemMenu1').show();
            $('#itemMenu').hide();
            $('#shopMenu1').show();
            $('#shopMenu').hide();
            $('#transaksiMenu1').show();
            $('#transaksiMenu').hide();
        }

    </script>

</body>
</html>
