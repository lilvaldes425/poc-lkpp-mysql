@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if  (Auth::user()->role_id == 4) 
            <a href="{{ route('home') }}">Data User</a> <i class="fa fa-chevron-right"></i> Edit User
            </br></br>
            @endif

            @if  (Auth::user()->role_id != 4) 
            <a href="{{ route('home') }}">Daftar User</a> <i class="fa fa-chevron-right"></i> Edit User
            </br></br>
            @endif

            <div class="panel panel-default">


                <div class="panel-heading">Edit User</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('edit.User') }}">
                        {{ csrf_field() }}

                        <input type="hidden" class="form-control" name="id" value="{{ $user->id }}">

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Username</label>

                             <div class="col-md-6">
                                <input type="text" class="form-control" name="username" value="{{ $user->username }}">

                                @if ($errors->has('username'))
                                   <span class="help-block">
                                      <strong>{{ $errors->first('username') }}</strong>
                                   </span>
                                @endif
                             </div>
                         </div>
                        
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password<br>
                                <span style="font-size: smaller; color: grey;">(kosongkan bila tidak berubah)</span>
                            </label>
                            

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">
                                <span id="msg_pass" style="display: none; color: red; font-size: small;">Password tidak sama!</span>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address" class="col-md-4 control-label">Address</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control" name="address" value="{{ $user->address }}" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="col-md-4 control-label">Phone Number</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ $user->hp }}" required>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('npwp') ? ' has-error' : '' }}">
                            <label for="npwp" class="col-md-4 control-label">NPWP</font></label>

                            <div class="col-md-6">
                                <input id="npwp" type="text" class="form-control" name="npwp" value="{{ $user->npwp }}" required>
                                @if ($errors->has('npwp'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('npwp') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        @if  (Auth::user()->role_id != 1) 
                            <div style="display:none">
                        @endif

                        <div class="form-group">
                            <label for="role" class="col-md-4 control-label">Jabatan</label>

                            <div class="col-md-6">
                                <select id="jabatan" type="jabatan" class="form-control" name="jabatan" value="{{ $user->jabatan_id }}" required>
                                    @foreach($jabatans as $jabatan)
                                        @if ($user->jabatan_id == $jabatan->id)
                                            <option value="{{$jabatan->id}}" selected>{{$jabatan->name}}</option>
                                        @else
                                            <option value="{{$jabatan->id}}">{{$jabatan->name}}</option>
                                        @endif      
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="role" class="col-md-4 control-label">Role</label>

                            <div class="col-md-6">
                                <select id="role" type="role" class="form-control" name="role" value="{{ $user->role_id }}" required>
                                    @foreach($roles as $role)
                                        @if ($user->role_id == $role->id)
                                            <option value="{{$role->id}}" selected>{{$role->name}}</option>
                                        @else
                                            <option value="{{$role->id}}">{{$role->name}}</option>
                                        @endif      
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        @if  (Auth::user()->role_id != 1) 
                            </div>
                        @endif

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" onclick="return konfirmasi();">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">  

function konfirmasi() {
    if(confirm('Data sudah benar?')){

        var pass = $('#password').val();

        var konfirmasi = $('#password-confirm').val();

        if(pass != '') {
            if(pass !== konfirmasi) {
                $('#msg_pass').show();
                return false;
            } else {
                $('#msg_pass').hide();
            }
        }

        return true;    
        
    } else {
        return false;
    }
}

</script>

@endsection
