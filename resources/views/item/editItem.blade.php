@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <a href="{{ route('listItem') }}">Daftar Barang</a> <i class="fa fa-chevron-right"></i> Edit Barang
            </br></br>
            
            <div class="panel panel-default">


                <div class="panel-heading">Edit Barang</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('edit.Item') }}">
                        {{ csrf_field() }}

                        <input type="hidden" class="form-control" name="id" value="{{ $items->id }}">                        
                        
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nama</label>

                            <div class="col-md-6">
                                <input id="nama" type="text" class="form-control" name="nama" value="{{ $items->nama }}" required autofocus>

                                @if ($errors->has('nama'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nama') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="deskripsi" class="col-md-4 control-label">Deskripsi</label>

                            <div class="col-md-6">
                                <input id="deskripsi" type="text" class="form-control" name="deskripsi" value="{{ $items->deskripsi }}" required autofocus>

                                @if ($errors->has('deskripsi'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('deskripsi') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('harga') ? ' has-error' : '' }}">
                            <label for="harga" class="col-md-4 control-label">Harga</label>

                            <div class="col-md-6">
                                <input id="hargaf" type="text" class="form-control" name="hargaf" value="{{ $items->harga }}" onkeyup="numFormat()" required autofocus>
                                <input type="hidden" name="harga" id="harga" value="{{ $items->harga }}">

                                @if ($errors->has('harga'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('harga') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('stok') ? ' has-error' : '' }}">
                            <label for="stok" class="col-md-4 control-label">Stok</label>

                            <div class="col-md-6">
                                {{-- <input id="stok" type="text" class="form-control" name="stok" value="{{ $items->stok }}" required autofocus> --}}
                                <input type="number" id="stok" name="stok" step="1" min="0" width="20px" style="width: 50px; padding-left: 3px;" value="{{ $items->stok }}" required autofocus>

                                @if ($errors->has('stok'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('stok') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" onclick="return konfirmasi();">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">  

$(function() {
    numFormat();
});

function konfirmasi() {
    if(confirm('Data sudah benar?') ){
        return true;
    } else {
        return false;
    }
}

function numFormat() {
    var harga = $("#hargaf").val().replace(/[.,\s]/g,'');
    harga = harga.replace(/[^0-9]/, '');
    harga = harga.replace(/\b0+/g, '');
    $("#harga").val(harga);        
    harga = harga.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
    $("#hargaf").val(harga);
}

</script>

@endsection

