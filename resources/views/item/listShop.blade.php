@extends('layouts.app')

@section('content')

<style>

.dataTables_filter{
    display: none;
}

.form-group {
    margin-bottom: 1px;
}

</style>


<div class="container">
@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close"><span class="fas fa-times"></span></a>
        <p>{{ $message }}</p>
    </div>
@endif

    <br/>
    <h1 class="text-center">Daftar Barang</h1>
    <br/>        

    <div class="row">
        <div class="col-md-2" style="width: 160px">
        Filter Nama Barang
        </div>
        <div class="col-md-2" style="width: 150px">
            <input id="cari" type="text" class="form-control" name="cari" style="width: 190px;">
        </div>
    </div>
    <div class="row" style="margin-top: 20px;">
        <div class="col-md-2" style="width: 160px">
        Filter Harga
        </div>
    
        <div class="col-md-2" style="width: 50px">Min. </div>
        <div class="col-md-2" style="width: 150px">
            <input id="minf" type="text" class="form-control" name="min" onkeyup="numFormat()" style="width: 100px;">
            <input type="hidden" name="min" id="min">
        </div>
        <div class="col-md-2" style="width: 50px">Max. </div>
        <div class="col-md-2" style="width: 150px">
            <input id="maxf" type="text" class="form-control" name="max" onkeyup="numFormat()" style="width: 100px;">
            <input type="hidden" name="max" id="max">
        </div>
    </div>
    <div class="row" style="margin-top: 10px; padding-bottom: 20px; border-bottom: 1px solid #d2d2d2">
        <div class="col-md-2" style="width: 100px">
        <button id="btn_filter" type="submit" class="btn btn-primary" onclick="cari();">
            Tampilkan
        </button>
        </div>
    </div>
    <br>

    <div class="row">
        <div id="filterKat" class="dataTables_length"></div>
    </div>    

    <table class="table table-striped table-hover" id="items-table" style="background-color: white; display: none;">
        <thead>
            <tr>
                <th style="max-width: 20px;">No</th>
                <th>Nama</th>
                <th>Deskripsi</th>
                <th>Harga</th>
                <th>Stok</th>
                <th style="max-width: 100px;">Action</th>
            </tr>
        </thead>
    </table>

    <div class="row">                    
        <div id="katalog"></div>
    </div>

                    <div class="modal fade" id="modal_shop" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                        <div class="modal-dialog">
                        <div class="modal-content" style="width: 400px; margin: auto;">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title" id="myModalLabel">Beli Barang</h4>
                            </div>

                            <form class="form-horizontal" method="POST" action="{{ route('beli.Item') }}">
                            {{ csrf_field() }}
                        
                            <div class="modal-body" style="margin-bottom: 20px;">
                            <div style='text-align: center; font-size: 60px; background-color: lightgrey; border-radius: 8px; margin-bottom: 10px;'><i class='fa fa-shopping-bag'></i></div>
                                <div class="form-group">
                                    <label class="control-label col-xs-4" >Nama Barang</label>
                                    <div class="col-xs-8" style="margin-top: 5px;">
                                        <span id="nama"></span>
                                    </div>
                                </div>                                        
            
                                <div class="form-group">
                                    <label class="control-label col-xs-4" >Harga</label>
                                    <div class="col-xs-8" style="margin-top: 5px;">
                                        <span id="harga"></span>
                                    </div>                                    
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-xs-4" >Stok</label>
                                    <div class="col-xs-8" style="margin-top: 5px;">
                                        <span id="stok"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-xs-4" >Jumlah Pembelian</label>
                                    <div class="col-xs-8" style="margin-top: 5px;">
                                        <input type="number" id="jumlah" name="jumlah" step="1" min="1" width="20px" style="width: 50px; padding-left: 3px;">
                                    </div>
                                </div>

                                <input type="hidden" name="id_user" id="id_user" value="{{ Auth::user()->id }}">
                                <input type="hidden" name="id_items" id="id_items">
            
                            </div>
                            <div style=" margin-left: 30px;"><span>Tekan <b>Beli</b> untuk melanjutkan pembelian</span></div>
            
                            <div class="modal-footer">
                                <input class="btn btn-primary btn-danger" type="button" value="Batal" data-dismiss="modal"/>
                                <input class="btn btn-primary" type="submit" value="Beli" onclick="return konfirmasi()"/>
                            </div>

                            </form>
                            
                        </div>
                        </div>
                    </div>

    <script>
    var table;

    $(function() {
        table = $('#items-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('getItems.datas') !!}',
            columns: [
                { data: 'no', name: 'no' , searchable: true},
                { data: 'nama', name: 'nama' , searchable: true},
                { data: 'deskripsi', name: 'deskripsi' , searchable: true},
                { data: null, name: 'harga', searchable: true, render: function ( data, type, row ) {
                    katalog(data, data.tot);
                    //alert(row);
                    return data.harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                } },
                { data: 'stok', name: 'stok' , searchable: true},                
                { data: null, name: 'action', render: function ( data, type, row ) {
                    if(data.stok == 0)
                        return "<b>Stok Habis</b>";
                    else 
                        return '<button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#modal_shop" onclick="pilih(\'' + data.id + '\', \'' + data.id_user + '\', \'' + data.nama + '\', \'' + data.harga + '\', \'' + data.stok + '\')"><i class="fa fa-shopping-cart"></i> Beli</button>';
                } },
            ],
            columnDefs: [{
                "defaultContent": "-",
                "searchable": false,
                "orderable": false,
                "targets": 0
              }],
            iDisplayLength: 20,
            initComplete: function(settings, json) {
                //alert( 'DataTables has finished its initialisation.' );
                //copyKat();
            }
        });

        $('#minf').keypress(function(e){
            if(e.keyCode==13)
            $('#btn_filter').click();
        });

        $('#maxf').keypress(function(e){
            if(e.keyCode==13)
            $('#btn_filter').click();
        });

        $('#cari').keypress(function(e){
            if(e.keyCode==13)
            $('#btn_filter').click();
        });
        
    });

    function reload_table(){
        table.ajax.reload(null,false); //reload datatable ajax
    }

    function cari() {
        var min = $("#min").val();
        var max = $("#max").val();
        var cari = $("#cari").val();

        if(max != '' && min > max) {
            alert('Harga minimum tidak boleh melebihi harga maksimum!')
        } else {
            table.ajax.url("{!! route('getItems.datas') !!}" + "?min=" + min + "&max=" + max + "&cari=" + cari);
            table.ajax.reload();
        }
    }

    var x = 0;
    var n = 1;
    var len = 0;
    var init = true;

    function katalog(data, tot) {
        //alert(JSON.stringify(table.page.info()));
        //var len = table.page.info().length;
        var harga = data.harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        var tombol;
        if(data.stok == 0)
            tombol = '<button type="button" class="btn btn-sm btn-danger btn-block" disabled><b>Stok Habis</b></button>';
        else 
            tombol = '<button type="button" class="btn btn-sm btn-success btn-block" data-toggle="modal" data-target="#modal_shop" ' + 
                    'onclick="pilih(\'' + data.id + '\', \'' + data.id_user + '\', \'' + data.nama + '\', \'' + data.harga + 
                    '\', \'' + data.stok + '\')"><i class="fa fa-shopping-cart"></i> <b>Beli</b></button>';
                        
        var cnt = $('select[name="items-table_length"] option:selected').val();
        //alert(cnt);
        var html = "<div class='col-md-3'><div class='panel panel-default' style='text-align: left; padding: 15px;'>" + 
                    "<div style='text-align: center; font-size: 60px; background-color: lightgrey; border-radius: 8px; margin-bottom: 10px;'>" + 
                    "<i class='fa fa-shopping-bag'></i></div>" + data.nama + "<br><b>Rp." + harga + "</b><br>Stok: " + 
                    data.stok + "<div style='margin-top: 20px; text-align: center;'>" + tombol + "</div></div></div>";

        if(init) {
            if(x >= cnt || x >= tot) {
                if(n > len) {
                    $("#katalog").html('');
                    n = 1;
                    len = tot;
                }
                $("#katalog").append(html);
                n++;
                init = false;
            }
        } else {
            if(n > len) {
                $("#katalog").html('');
                n = 1;
                len = tot;
            }
            $("#katalog").append(html);
            n++;
        }
                
        x++;        
    }

    function confirmDel(id, name) {
        var txt;
        var r = confirm("Yakin akan menghapus data? \n\nJabatan: " + name);
        if (r == true) {
            txt = "You pressed OK!"; 
            $('#delButton'+id).click();
        } else {
            txt = "You pressed Cancel!";
        }
    }

    function pilih(id_items, id_user, nama, harga, stok) {
        $("#nama").html(nama);
        $("#harga").html("Rp." + harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        $("#stok").html(stok);
        $("#jumlah").val(1);
        $("#jumlah").attr('max', stok);
        $("#id_items").val(id_items);
        //$("#id_user").val(id_user);
    }

    function copyKat() {
        var html = $("#items-table_length").html();
        //alert(html);        
        $("#filterKat").html(html);
    }

    function konfirmasi() {
        if(confirm('Yakin akan membeli barang ini?') ){
            return true;
        } else {
            return false;
        }
    }

    $(document).ready(function() {
        $('select[name="items-table_length"]').html('<option value="20">20</option><option value="40">40</option><option value="100">100</option>');
    });    

    function numFormat() {
        var min = $("#minf").val().replace(/[.,\s]/g,'');
        min = min.replace(/[^0-9]/, '');
        min = min.replace(/\b0+/g, '');
        $("#min").val(min);        
        min = min.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        
        $("#minf").val(min);

        var max = $("#maxf").val().replace(/[.,\s]/g,'');
        max = max.replace(/[^0-9]/, '');
        max = max.replace(/\b0+/g, '');
        $("#max").val(max);        
        max = max.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        
        $("#maxf").val(max);
    }

    </script>
    @stack('scripts')

</div>
@endsection
