@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <a href='listItem'>Daftar Barang</a> <i class="fa fa-chevron-right"></i> Tambah Barang
            </br></br>
            <div class="panel panel-default">

                <div class="panel-heading">Tambah Barang</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('add.Item') }}">
                        {{ csrf_field() }}
                        
                        <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                            <label for="nama" class="col-md-4 control-label">Nama<font color="red"> *</font></label>

                            <div class="col-md-6">
                                <input id="nama" type="text" class="form-control" name="nama" value="{{ old('nama') }}" required>

                                @if ($errors->has('nama'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nama') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                            <label for="deskripsi" class="col-md-4 control-label">Deskripsi<font color="red"> *</font></label>

                            <div class="col-md-6">
                                <input id="deskripsi" type="text" class="form-control" name="deskripsi" value="{{ old('deskripsi') }}" required>

                                @if ($errors->has('deskripsi'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('deskripsi') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('harga') ? ' has-error' : '' }}">
                            <label for="harga" class="col-md-4 control-label">Harga<font color="red"> *</font></label>

                            <div class="col-md-6">
                                <input id="hargaf" type="text" class="form-control" name="hargaf" value="{{ old('harga') }}" onkeyup="numFormat()" required>
                                <input type="hidden" name="harga" id="harga">

                                @if ($errors->has('hargaf'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('hargaf') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('stok') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Stok<font color="red"> *</font></label>

                            <div class="col-md-6">
                                {{-- <input id="stok" type="text" class="form-control" name="stok" value="{{ old('stok') }}" required> --}}
                                <input type="number" id="stok" name="stok" step="1" min="0" width="20px" style="width: 50px; padding-left: 3px;" value="{{ old('stok') }}" required>

                                @if ($errors->has('stok'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('stok') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" onclick="return konfirmasi();">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">  

    function konfirmasi() {
        if(confirm('Data sudah benar?') ){
            return true;
        } else {
            return false;
        }
    }

    function numFormat() {
        var harga = $("#hargaf").val().replace(/[.,\s]/g,'');
        harga = harga.replace(/[^0-9]/, '');
        harga = harga.replace(/\b0+/g, '');
        $("#harga").val(harga);        
        harga = harga.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        
        $("#hargaf").val(harga);
    }

    </script>
</div>

@endsection

