@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <a href='listRole'>Daftar Role</a> <i class="fa fa-chevron-right"></i> Tambah Role
            </br></br>
            <div class="panel panel-default">

                <div class="panel-heading">Tambah Role</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('add.Role') }}">
                        {{ csrf_field() }}
                        
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name<font color="red"> *</font></label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" onclick="return konfirmasi();">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

<script type="text/javascript">  

function konfirmasi() {
    if(confirm('Data sudah benar?') ){
        return true;
    } else {
        return false;
    }
}

</script>