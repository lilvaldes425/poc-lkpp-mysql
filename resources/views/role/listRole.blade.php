@extends('layouts.app')

@section('content')




<div class="container">
@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

    <br/>
    <h1 class="text-center">Daftar Role</h1>
    <br/>
    <a href="{{route('addRole.page')}}" class="btn btn-xs btn-primary"><i class="fa fa-plus-circle"></i> Tambah Role</a>    
    <br/><br/>
    <table class="table table-striped table-hover" id="roles-table" style="background-color: white;">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>

    <script>
    $(function() {

        var table = $('#roles-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('getRoles.datas') !!}',
            columns: [
                { data: 'no', name: 'no' , searchable: true},
                { data: 'name', name: 'name' , searchable: true},                
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ],
            columnDefs: [{
                "defaultContent": "-",
                "searchable": false,
                "orderable": false,
                "targets": 0
              }]
        });
        
    });

    function confirmDel(id, name) {
        var txt;
        var r = confirm("Yakin akan menghapus data? \n\nRole: " + name);
        if (r == true) {
            txt = "You pressed OK!"; 
            $('#delButton'+id).click();
        } else {
            txt = "You pressed Cancel!";
        }
    }

    </script>
    @stack('scripts')

</div>
@endsection
