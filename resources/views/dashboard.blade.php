@extends('layouts.app')

@section('content')


<div class="container">

    <center><h1><i style="margin-left: -13px" class="fa fa-apple-alt"></i> &nbsp;Dashboard</h1></center>
    <br><br>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        
        <div class="row">
            <div class="col-md-3" style="background-color:#333333;border-radius: 5px !important;">
                <div class="card">
                    <div class="card-title">
                        <center><h3 style="color:white;">Total User</h3></center>
                    </div>
                    <div class="card-body">
                        <center><h1 style="color:white;"><?=$users?></h1><span style="color:white;">Orang</span></center><br>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3" style="background-color:#333333;border-radius: 5px !important;">
                <div class="card">
                    <div class="card-title">
                        <center><h3 style="color:white;">Total Barang</h3></center>
                    </div>
                    <div class="card-body">
                        <center><h1 style="color:white;"><?=$barangs?></h1><span style="color:white;">Barang</span></center><br>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3" style="background-color:#333333;border-radius: 5px !important;">
                <div class="card">
                    <div class="card-title">
                        <center><h3 style="color:white;">Total Transaksi</h3></center>
                    </div>
                    <div class="card-body">
                        <center><h1 style="color:white;"><?=$transaksis?></h1><span style="color:white;">Transaksi</span></center><br>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    
    <br>
    <br>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
            <script type="text/javascript">
                var json = <?php echo $json; ?>;
                google.charts.load('current', {'packages':['corechart']});
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {
                    var data = google.visualization.arrayToDataTable(json);

                    var options = {
                    title: 'Perkembangan Transaksi',
                    curveType: 'function',
                    legend: { position: 'bottom' }
                    };

                    var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

                    chart.draw(data, options);
                }
            </script>
            <div id="curve_chart" style="width: 680px; height: 300px"></div>

            <br>
            <script type="text/javascript">
                var jsons = <?php echo $jsons; ?>;
                function drawChart() {
                    // Define the chart to be drawn.
                    var data = google.visualization.arrayToDataTable(jsons);

                    var options = {title: 'Perkembangan User'}; 

                    // Instantiate and draw the chart.
                    var chart = new google.visualization.BarChart(document.getElementById('container'));
                    chart.draw(data, options);
                }
                google.charts.setOnLoadCallback(drawChart);
            </script>
            <div id = "container" style="width: 680px; height: 300px"></div>

            <br>
            <script type="text/javascript">
                var jsonss = <?php echo $jsonss; ?>;
                google.charts.load('current', {'packages':['corechart']});
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {

                    var data = google.visualization.arrayToDataTable(jsonss);

                    var options = {
                    title: 'Stok Barang'
                    };

                    var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                    chart.draw(data, options);
                }
            </script>
            <div id="piechart" style="width: 680px; height: 300px"></div>
        </div>
    </div>
    
</div>


@endsection
