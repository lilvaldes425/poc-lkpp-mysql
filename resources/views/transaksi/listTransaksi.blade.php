@extends('layouts.app')

@section('content')




<div class="container">
@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close"><span class="fas fa-times"></span></a>
        <p>{{ $message }}</p>
    </div>
@endif

    <br/>
    <h1 class="text-center">Riwayat Transaksi</h1>
    <br/>
    <a href="{{route('get.excelTransaksi')}}" class="btn btn-xs btn-primary"><i class="fa fa-download"></i> Export to Excel</a>
    <br/><br/>
    <table class="table table-striped table-hover" id="transaksi-table" style="background-color: white;">
        <thead>
            <tr>
                <th style="max-width: 20px;">No</th>
                <th>Jenis</th>
                <th>Nama Pembeli</th>
                <th>Nama Barang</th>
                <th>Harga</th>
                <th>Jumlah</th>
                <th>Tanggal</th>
                <!-- <th style="max-width: 100px;">Action</th> -->
            </tr>
        </thead>
    </table>

    <script>
    $(function() {

        var table = $('#transaksi-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('getTransaksi.datas') !!}',
            columns: [
                { data: 'no', name: 'no' , searchable: true},
                { data: 'jenis', name: 'jenis' , searchable: true},
                { data: 'nama_user', name: 'nama_user' , searchable: true},
                { data: 'nama_item', name: 'nama_item' , searchable: true},
                { data: null, name: 'harga', searchable: true, render: function ( data, type, row ) {
                    return data.harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                } },
                { data: 'jumlah_items', name: 'jumlah_items' , searchable: true},
                { data: 'created_date', name: 'created_date' , searchable: true},
                // { data: null, name: 'action', render: function ( data, type, row ) {
                //     return '<a href="editItemPage/' + data.id + '" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a>' +  
                //     '<a href="#delete-' + data.id + '" class="btn btn-xs btn-primary" style="margin-left: 5px;" onclick=\'confirmDel(' + data.id + ',"' + data.nama + '")\'><i class="fa fa-trash"></i> Delete</a>' +
                //     '<form class="delete" action="delItem/' + data.id + '" method="get">' + 
                //     '<input type="hidden" class="form-control" name="id" value="' + data.id + '">' +                 
                //     '<input id="delButton' + data.id + '" type="submit" value="Delete" style="display: none;">' + 
                //     '</form></div>';
                // } },
            ],
            columnDefs: [{
                "defaultContent": "-",
                "searchable": false,
                "orderable": false,
                "targets": 0
              }]
        });
        
    });

    function confirmDel(id, name) {
        var txt;
        var r = confirm("Yakin akan menghapus data? \n\nJabatan: " + name);
        if (r == true) {
            txt = "You pressed OK!"; 
            $('#delButton'+id).click();
        } else {
            txt = "You pressed Cancel!";
        }
    }

    </script>
    @stack('scripts')

</div>
@endsection
