<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role_id == 2){
        return redirect()->route('profile')->with('success','Anda Berhasil Registrasi');
        }else{
            return view('home');
        }
    }

    public function dashboard(){
        $users = DB::table('users')->count();
        $barangs = DB::table('items')->count();
        $transaksis = DB::table('transaksis')->count();
        
        $trans = DB::select("SELECT DATE_FORMAT(created_date,'%y-%m-%d') created_date, SUM(jumlah_items) jumlah_items FROM `transaksis` GROUP BY created_date");

        $result[] = ['Tanggal','Barang di Beli'];
        foreach ($trans as $key => $value) {
            $result[++$key] = [$value->created_date, (int)$value->jumlah_items];
        }

        $json = json_encode($result);


        $user = DB::select("SELECT DATE_FORMAT(created_at,'%y-%m-%d') created_date, COUNT(*) jumlah_items FROM `users` GROUP BY created_at");
        $results[] = ['Tanggal','User Terdaftar'];
        foreach ($user as $key => $value) {
            $results[++$key] = [$value->created_date, (int)$value->jumlah_items];
        }
        $jsons = json_encode($results);

        $barang = DB::table('items')->select ('*')->get();
        $resultss[] = ['Barang','Stok'];
        foreach ($barang as $key => $value) {
            $resultss[++$key] = [$value->nama, (int)$value->stok];
        }
        $jsonss = json_encode($resultss);


        return view('dashboard', compact('users','barangs','transaksis','json','jsons','jsonss'));
    }
}
