/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : localhost:3306
 Source Schema         : poc-lkpp

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 31/01/2020 10:25:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for items
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `deskripsi` varchar(100) DEFAULT NULL,
  `harga` float DEFAULT NULL,
  `stok` int(10) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of items
-- ----------------------------
BEGIN;
INSERT INTO `items` VALUES (1, 'Laptop 1', 'Laptop nomor 1', 5000000, 11, NULL, '2019-02-27 09:29:14');
INSERT INTO `items` VALUES (2, 'Komputer', 'Komputer baru', 1000, 10, NULL, NULL);
INSERT INTO `items` VALUES (3, 'Meja', 'meja belajar', 500000, 5, NULL, NULL);
INSERT INTO `items` VALUES (4, 'Lemari', 'lemari dokumen', 1000000, 2, NULL, NULL);
INSERT INTO `items` VALUES (5, 'Monitor', 'monitor LED', 2500000, 15, NULL, NULL);
INSERT INTO `items` VALUES (6, 'Mouse', 'mouse kecil', 200000, 20, NULL, NULL);
INSERT INTO `items` VALUES (7, 'Server', 'server development', 10000000, 2, NULL, NULL);
INSERT INTO `items` VALUES (8, 'Mobil', 'mobil biasa', 100000000, 1, NULL, NULL);
INSERT INTO `items` VALUES (10, 'Modem', 'modem 4G', 200000, 30, NULL, NULL);
INSERT INTO `items` VALUES (11, 'Router', 'router saja', 300000, 10, NULL, NULL);
INSERT INTO `items` VALUES (12, 'Handphone', 'hp merek lokal', 10000000, 40, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for jabatans
-- ----------------------------
DROP TABLE IF EXISTS `jabatans`;
CREATE TABLE `jabatans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of jabatans
-- ----------------------------
BEGIN;
INSERT INTO `jabatans` VALUES (1, 'Manajer', NULL, NULL);
INSERT INTO `jabatans` VALUES (2, 'Karyawan', NULL, NULL);
INSERT INTO `jabatans` VALUES (3, 'ob', NULL, NULL);
INSERT INTO `jabatans` VALUES (4, 'User', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2018_01_30_033034_create_roles_table', 1);
INSERT INTO `migrations` VALUES (4, '2014_10_12_000000_create_users_table', 2);
INSERT INTO `migrations` VALUES (5, '2018_02_08_074032_create_jabatans_table', 3);
COMMIT;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
BEGIN;
INSERT INTO `roles` VALUES (1, 'ADMIN', '2018-01-30 11:31:19', '2018-01-30 11:31:22');
INSERT INTO `roles` VALUES (2, 'USER', '2018-01-30 11:31:36', '2018-01-30 11:31:39');
INSERT INTO `roles` VALUES (3, 'CREATE', NULL, NULL);
INSERT INTO `roles` VALUES (4, 'DETAIL', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for transaksis
-- ----------------------------
DROP TABLE IF EXISTS `transaksis`;
CREATE TABLE `transaksis` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `id_user` int(10) DEFAULT NULL,
  `id_items` int(10) DEFAULT NULL,
  `jumlah_items` int(10) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `nama_user` varchar(100) DEFAULT NULL,
  `nama_item` varchar(100) DEFAULT NULL,
  `harga` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hp` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `npwp` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `jabatan_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 'Administrator', 'admin1', 'admin1@bgs.com', 'Jakarta', '0888888888888', '11.111.111.1-111.111', '$2y$10$7BFDVTq.3wiEN7KmoZ3z4.FxjR6b2l4Of006pY3AHwSB8K5rxOadu', 'Iin1Xc5L5H91MN54HMyjO7voYPvu3sSB9RDTsSquB7AiKHkrh8Fgv8B05SHO', '2018-02-05 04:45:14', '2018-02-05 04:45:14', 1, 1);
INSERT INTO `users` VALUES (34, 'User 1', 'user1', 'user1@bgs.com', 'Jakarta', '0888888888888', '11.111.111.1-111.111', '$2y$10$MaXSr1mtE/nVi860DMYiTOB/JWmAWy4zWGo/a5Bdth3pi069VRc1C', 'qZCKuofkEfy2LA8AOSgl5Gp27TPdEBxa7hVtl1ycgHkmaLrjrGhNkGyq4zkv', '2020-01-31 10:19:23', '2020-01-31 10:19:23', 2, 4);
INSERT INTO `users` VALUES (35, 'Karyawan 1', 'karyawan1', 'karyawan1@bgs.com', 'Jakarta', '0888888888888', '11.111.111.1-111.111', '$2y$10$W28/9FFLi/sUu3d0e6j6Le4O7b79/ThR9ZV36ZzGSo8p3YKf42Bz2', 'vy0GnQpmCVYsfIjA4oKlpdCDsn5xbmWTQVKqAegcyLQCSqu7Y7Uj4ysopC2z', '2020-01-31 10:20:50', '2020-01-31 10:20:50', 3, 2);
INSERT INTO `users` VALUES (36, 'Manajer 1', 'manajer1', 'manajer1@bgs.com', 'Jakarta', '0888888888888', '11.111.111.1-111.111', '$2y$10$8Omzc1O2X7QNIPtffDih/uDUh0s9ZVntAaQzH7z6HVAtt/z1oKIHe', 'HOPszrnK670WQKpsIbdIvWsMgpM1quG2aiP5iGMqLegpVUI47egbdNLziAAO', '2020-01-31 10:21:23', '2020-01-31 10:21:23', 1, 1);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
